var dvds = [
    {
        name:'Les Évadés',
        genre:'Fantastique',
        casting:['Tim Robbins', 'Morgan Freeman', 'Bob Gunton'],
        price:'8.50',
        year: '1994'
    },
    {
        name:'Le Parrain',
        type:'Mafia',
        recipe:['Marlon Brando', 'Al Pacino', 'James Caan'],
        price:'6.50',
        year: '1972'
    },
    {
        name:'Fight Club',
        type:'Psychologique',
        recipe:['Brad Pitt', 'Edward Norton', 'Meat Loaf'],
        price:'7.50',
        year: '1999'
    }
];