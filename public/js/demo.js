$(document).ready(function() {
    $('#createForm').on('submit', function(e) {
        e.preventDefault();
        var $this = $(this);
        var genre = $('#genre');
        var casting = $('#casting');
        var price = $('#price');
        var name = $('#name');
        var year = $('#year');

        if(genre === '' || casting === '' || price === '' || name === '' || year === '') {
            alert('Les champs doivent être remplis');
        }
        else
        {
            $.ajax({
                url: $this.attr('action'),
                type: $this.attr('method'),
                data: $this.serialize(),
                success: function(html) {
                    if (html["status"] === "ok") {
                        addItem(
                            {
                                name:   name.val(),
                                casting: casting.val(),
                                price:  price.val(),
                                year:   year.val(),
                                genre:  genre.val()
                            },
                            html["id"]
                        );
                        genre.children('option[value=""]').prop('selected', true);
                        casting.val('');
                        price.val('');
                        name.val('');
                        year.children('option[value="2020"]').prop('selected', true);
                    } else {
                        $('#errorField').html(html["value"]);
                        $('.errorModal').modal("show");
                    }
                }
            });
        }
    });

    $('.updateForm').on('submit', function(e) {
        e.preventDefault();
        var $this = $(this);
        var id = $(this).attr('attr-id');
        $.ajax({
            url: '/dvd/' + id,
            type: 'PUT',
            data: $this.serialize(),
            success: function (html) {
                var base = $('#list-' + id);
                var baseUpdate = $('#update-' + id);
                html = JSON.parse(html);
                console.log(html);
                console.log(html.status);
                console.log(html.dvd);
                console.log(html.dvd.name);
                base.children().children(".title").html(html.dvd.name);
                base.removeClass("hidden");
                baseUpdate.addClass("hidden");
            }
        });
    });
});

function addItem(value, id) {
    var list = $('#list');

    var newli = "<li class='list-group-item' id='list-" + id + "'>" +
                    "<div align='right' style='float:right'>" +
                        "<button class='btn btn-xs' id='btn-" + id + "' onClick='preUpdate(this.id)'>" +
                            "Update" +
                        "</button>" +
                        "<button class='btn btn-danger btn-xs' id='btn-" + id + "' onClick='deleteItem(this.id)'>" +
                            "Delete" +
                        "</button>" +
                    "</div>" +
                   "<div align='left'><h4>" + value.name + "</h4></div>" +
                   "<div align='left'>" + value.genre + "</div>" +
                   "<div align='left'>" + value.year + "</div>" +
                   "<div align='left'>" + value.casting + "</div>" +
                "</li>";
    list.append(newli);
}

function preUpdate(clicked_id) {
    var id = clicked_id.substr(4);
    var base = $('#list-' + id);
    var baseUpdate = $('#update-' + id);

    base.addClass("hidden");
    baseUpdate.removeClass("hidden");
}

function deleteItem(clicked_id) {
    var id = clicked_id.substr(4);
    $.ajax({
        url: '/dvd/' + id,
        type: 'DELETE',
        success: function(html) {
            if (html["status"] === "ok") {
                var item1 = $('#list-' + html["value"]);
                var item2 = $('#update-' + html["value"]);
                item1.remove();
                item2.remove();
            } else {
                $('#errorField').html(html["value"]);
                $('.errorModal').modal("show");
            }
        }
    });
}