var express = require('express');
var mongodb = require('../db');

var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  mongodb.getDvds(res);
});

router.post('/dvd', function(req, res) {
  res.setHeader('Content-Type', 'application/json');

  if (req.body === undefined || req.body === "") {
    res.send(JSON.stringify({status: "error", value: "Value undefined"}));
    return
  }
  mongodb.sendDvd(req.body, res);
});

/* DELETE item. */
router.delete('/dvd/:id', function(req, res) {
  res.setHeader('Content-Type', 'application/json');
  var id = req.param('id');

  if (id === undefined || id === "") {
    res.send(JSON.stringify({status: "error", value: "Id undefined"}));
    return
  }
  mongodb.delVal(id);
  res.send(JSON.stringify({status: "ok", value: id}));
});

/* PUT item */
router.put('/dvd/:id', function (req, res) {
    var id = req.param('id');

    if (id === undefined || id === "") {
        res.send(JSON.stringify({status: "error", value: "Id undefined"}));
        return
    }
    if (req.body === undefined || req.body === "") {
        res.send(JSON.stringify({status: "error", value: "Value undefined"}));
        return
    }
    mongodb.updateDvd({
          name: req.body.name,
          genre: req.body.genre,
          casting: JSON.stringify(req.body.casting.split(',')),
          price: req.body.price,
          year: req.body.year,
          id: id
        }, res);
});

module.exports = router;
