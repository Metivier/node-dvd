var mongoose = require('mongoose');

var dvdSchema = mongoose.Schema({
    name: String,
    genre: String,
    casting: String,
    price: String,
    year: String
});
var Dvds = mongoose.model('dvds', dvdSchema);
var ObjectId = require('mongodb').ObjectId;


module.exports = {
    connectDB : function() {
		// Connect to Mongo DB
		mongoose.connect('mongodb://localhost/dvds'); // local
    },

    getDvds : function(res) {
        Dvds.find(function(err, result) {
            if (err) {
                console.log(err);
                res.send('database error');
                return
            }
            for(var i in result) {
                result[i]._id = ObjectId(result[i]._id);
            }
            res.render('index', {title: 'DVD Library', dvds: result});
        });
    },

    sendDvd : function(val, res) {
        var request = new Dvds({
                name: val['name'],
                genre: val['genre'],
                casting: JSON.stringify(val['casting'].split(',')),
                price: val['price'],
                year: val['year']
            });
        request.save(function (err, result) {
            if (err) {
                console.log(err);
                    res.send(JSON.stringify({
                        status: "error",
                        value: "Error, db request failed"
                    }));
                return
            }
            res.status(201).send(JSON.stringify({status: "ok",  id: ObjectId(result['id'])}));
        });
    },

    delVal : function(id) {
        Dvds.deleteOne({_id: id}, function(err) {
            if (err) {
                console.log(err);
            }
        });
    },

    updateDvd : function(val, res) {
        var request = Dvds.find({_id: val.id});
        console.log(val);
        request.update({_id: val.id}, val, function (err, result) {
            if (err) {
                console.log(err);
                res.send(JSON.stringify({
                    status: "error",
                    value: "Error, db request failed"
                }));
                return
            }
            res.status(200).send(JSON.stringify({status: "ok",  dvd: val}));
        });
    }
};
